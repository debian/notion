Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Notion
Upstream-Contact: Arnout Engelen <arnout@bzzt.net>
Source: https://notionwm.net
Files-Excluded: contrib/scripts/notion_rofi.sh
                contrib/scripts/rofi.lua
                contrib/statusd/statusd_inetaddr2/test_statusd_inetaddr2.lua
                contrib/keybindings/cfg_mouse.lua
                contrib/keybindings/dans_bindings.lua
                contrib/keybindings/vim_bindings.lua
                contrib/scripts/app.lua
                contrib/scripts/autoprop.lua
                contrib/scripts/bookmarks.lua
                contrib/scripts/cfg_dock2.lua
                contrib/scripts/collapse.lua
                contrib/scripts/float-sb.lua
                contrib/scripts/legacy/adapt_menus.lua
                contrib/scripts/legacy/bindsearch.lua
                contrib/scripts/legacy/closeorkill.lua
                contrib/scripts/legacy/cwin_sp.lua
                contrib/scripts/legacy/document_menus.lua
                contrib/scripts/legacy/environment_placement_hook.lua
                contrib/scripts/legacy/goto_multihead.lua
                contrib/scripts/legacy/heuristics.lua
                contrib/scripts/legacy/histcompl.lua
                contrib/scripts/legacy/nextact.lua
                contrib/scripts/legacy/notifybox.lua
                contrib/scripts/lock_frame.lua
                contrib/scripts/min_tabs.lua
                contrib/scripts/move_current.lua
                contrib/scripts/mp.lua
                contrib/scripts/mpd.lua
                contrib/scripts/panel.lua
                contrib/scripts/send_to_ws.lua
                contrib/scripts/tabmenu.lua
                contrib/statusbar/legacy/statusbar_workspace.lua
                contrib/statusbar/statusbar_fname.lua
                contrib/statusd/legacy/statusd_mcpu.lua
                contrib/statusd/legacy/statusd_sysmon.lua
                contrib/statusd/statusd_amarok.lua
                contrib/statusd/statusd_dgs.lua
                contrib/statusd/statusd_mpd-socket.lua
                contrib/statusd/statusd_mpd.lua
                contrib/statusd/statusd_orpheus.lua
                contrib/statusd/statusd_pytone.lua
                contrib/statusd/statusd_ticker.lua
                contrib/styles/look_alex.lua
                contrib/styles/look_asm.lua
                contrib/styles/look_awesome.lua
                contrib/styles/look_awesome_sm.lua
                contrib/styles/look_awesome_yaarg.lua
                contrib/styles/look_bas.lua
                contrib/styles/look_blue.lua
                contrib/styles/look_bluecurve.lua
                contrib/styles/look_cleanpastel.lua
                contrib/styles/look_cleansteel.lua
                contrib/styles/look_cleansteel_trans.lua
                contrib/styles/look_cleanwhite.lua
                contrib/styles/look_cool.lua
                contrib/styles/look_gtk2.lua
                contrib/styles/look_minimalist.lua
                contrib/styles/look_moy.lua
                contrib/styles/look_ootput.lua
                contrib/styles/look_ootput_dark.lua
                contrib/styles/look_outback.lua
                contrib/styles/look_qt.lua
                contrib/styles/look_tiny.lua
                contrib/styles/look_tiny_min_tabs.lua
                contrib/styles/look_whitecode.lua

Files: *
Copyright: 2010-2020 The Notion Team
           1999-2007 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: debian/*
Copyright: Tommi Virtanen <tv@debian.org>
           Per Olofsson <pelle@dsv.su.se>
           Ben Hutchings <benh@debian.org>
           2011-2014 Arnout Engelen <arnouten@bzzt.net>
           2011-2014 Philipp Hartwig <ph@phhart.de> 
           2015-2017 Dima Kogan <dkogan@debian.org>
License: LGPL-2.1+

Files: contrib/keybindings/emacs_bindings.lua
Copyright: 2005-2007 Matthieu MOY <Matthieu.Moy@imag.fr>
           2005-2007 David Hansen <david.hansen@physik.fu-berlin.de>
           2005-2007 Tyranix <tyranix@gmail.com>
           2005-2007 Adam Di Carlo <aph@debian.org>
License: public-domain-emacs-bindings

Files: contrib/scripts/notsubtle.lua
Copyright: 2011 M Rawash <mrawash@gmail.com>
License: GPL-2+

Files: de/font.*
       notion/notion.c
       libmainloop/exec.c
Copyright: 2013 The Notion Team
           1999-2007 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: ioncore/framedpholder.c
Copyright: 2013 The Notion Team
           2005-2007 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: ioncore/ioncore.c
       ioncore/property.c
Copyright: 2011 The Notion Team
           1999-2007 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: ioncore/log.*
Copyright: 2013 The Notion Team
License: LGPL-2.1+

Files: ioncore/profiling.h
Copyright: 2012 The Notion Team
License: LGPL-2.1+

Files: mod_xinerama/mod_xinerama.c
Copyright: Thomas Themel <themel0r@wannabehacker.com>
           Tomas Ebenlendr <ebik@ucw.cz>
License: LGPL-2.1+

Files: mod_notionflux/*
Copyright: 2004-2005  Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: mod_dock/*
Copyright: 2003 Tom Payne
           2003 Per Olofsson
           2004-2009 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: utils/ion-completefile/*
Copyright: 1992 Simmule Turner, Rich Salz
License: BSD-completefile

Files: libtu/*
Copyright: 1999-2007 Tuomo Valkonen <tuomov@iki.fi>
License: Artistic_Clarified or LGPL-2.1+

Files: libtu/rb*
Copyright: 2000 James S. Plank <plank@cs.utk.edu>
           2004 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: libextl/*
Copyright: 2003-2005 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: libextl/install-sh
Copyright: 1991 Massachusetts Institute of Technology
License: MIT-old-style

Files: libtu/snprintf_2.2/*
Copyright: 1999 Mark Martinec
License: Artistic_Frontier

Files: de/fontset.c
Copyright: 2001-2002 Sean 'Shaleh' Perry <shaleh@debian.org>
           1997-2000 Brad Hughes <bhughes@tcac.net>
           2013 The Notion team
License: MIT

Files: ioncore/grab.*
Copyright: 2002 Lukas Schroeder
           2003-2007 Tuomo Valkonen <tuomov@iki.fi>
License: Artistic_Clarified or LGPL-2.1+

Files: mod_xkbevents/*
Copyright: 2006 Sergey Redin
           2011 Etan Reisner
License: MIT

Files: contrib/scripts/legacy/frame_client_menu.lua
       contrib/scripts/legacy/named_floating_groupws.lua
       contrib/scripts/named_scratchpad.lua
       contrib/scripts/net_client_list.lua
       contrib/scripts/nowarp_scratchpad.lua
       contrib/scripts/show_submap.lua
       contrib/scripts/simple_bindings.lua
       contrib/statusbar/statusbar_wsname.lua
Copyright: 2007 Etan Reisner <deryni@gmail.com>
License: MIT

Files: contrib/scripts/query_url.lua
Copyright: 2005 Reuben Thomas <rrt@sc3d.org>
License: GPL-2

Files: contrib/scripts/stock.lua
       contrib/scripts/weather.lua
Copyright: 2006 Andrea Rossato <arossato@istitutocolli.org>
License: GPL-2+

Files: contrib/scripts/legacy/xinerama_switcher.lua
Copyright: 2005 Martin F. Krafft <madduck@madduck.net>
License: Artistic

Files: contrib/scripts/xkbion.lua
Copyright: Sergey Redin <sergey@redin.info>
License: public-domain-xkbion

Files: contrib/statusbar/statusbar_act.lua
Copyright: 2006 Tuomo Valkonen <tuomov@iki.fi>
           2007 Etan Reisner
License: LGPL-2.1+

Files: contrib/statusbar/statusbar_external.lua
       contrib/statusd/statusd_exec.lua
Copyright: 2005 Antti Vähäkotamäki
License: LGPL-2.1+

Files: contrib/statusd/legacy/statusd_batt.lua
       contrib/statusd/statusd_cpuspeed.lua
       contrib/statusd/legacy/statusd_iface.lua
       contrib/statusd/statusd_inetaddr.lua
       contrib/statusd/statusd_iwinfo.lua
Copyright: 2004 Relu Patrascu <ikoflexer@gmail.com>
License: LGPL-2.1+

Files: contrib/statusd/statusd_binclock.lua
       contrib/statusd/statusd_flashing.lua
       contrib/statusd/statusd_mem.lua
       contrib/statusd/statusd_nmaild.lua
Copyright: 2006 Mario García H. <drosophila@nmental.com>
License: GPL-2

Files: contrib/statusd/statusd_bsdbatt.lua
Copyright: 2005 Russell A. Jackson
License: NetBSD

Files: contrib/statusd/statusd_laptopstatus.lua
Copyright: 2005 Jari Eskelinen <jari.eskelinen@iki.fi>
License: GPL-2

Files: contrib/statusd/statusd_maildir.lua
Copyright: 2005 Brett Parker <iDunno@sommitrealweird.co.uk>
License: GPL-2+

Files: contrib/statusd/statusd_weather.lua
Copyright: 2006 Andrea Rossato <arossato@istitutocolli.org>
           2008 Sergey Kozhemyakin <luc@server.by>
License: GPL-2+

Files: contrib/install-scripts.sh
Copyright: Dan Weiner <dbw@uchicago.edu>
License: public-domain

Files: contrib/scripts/goto_by_tag.lua
       contrib/scripts/wrap_workspace_or_screen.lua
Copyright: 2012 Philipp Hartwig <ph@phhart.de>
License: MIT

Files: contrib/scripts/legacy/alt_resize.lua
Copyright: Per Olofsson <pelle@dsv.su.se>
License: public-domain

Files: contrib/scripts/legacy/enumerate.lua
       contrib/scripts/rss_feed.lua
       contrib/scripts/legacy/ctrl_statusbar.lua
       contrib/scripts/exec_show.lua
       contrib/scripts/schedule.lua
       contrib/statusd/statusd_netmon.lua
       contrib/statusd/statusd_uptime.lua
       contrib/styles/look_atme.lua
Copyright: Sadrul Habib Chowdhury <imadil@gmail.com>
License: public-domain

Files: contrib/scripts/legacy/go_frame_or_desk.lua
Copyright: Rene van Bevern <rvb@pro-linux.de>
License: public-domain

Files: contrib/scripts/legacy/nest_ws.lua
Copyright: 2006 Matthieu Moy <matthieu.moy@imag.fr>
           2006 Etan Reisner <deryni@gmail.com>
License: public-domain

Files: contrib/scripts/legacy/rotate_statusbar.lua
       contrib/statusd/statusd_mocp.lua
       contrib/statusd/statusd_uname.lua
       contrib/verify_index.pl
Copyright: Tyranix <tyranix@gmail.com>
License: public-domain

Files: contrib/scripts/rss_feed_hh.lua
Copyright: Sadrul Habib Chowdhury <imadil@gmail.com>
           Henning Hasemann <hhasemann@web.de>
License: public-domain

Files: contrib/scripts/switch_bindings.lua
Copyright: Sadrul Habib Chowdhury <imadil@gmail.com>
           Canaan Hadley-Voth
License: public-domain

Files: contrib/scripts/zoom.lua
Copyright: Rene van Bevern <rvb@pro-linux.de>
           Etan Reisner <deryni@gmail.com>
License: public-domain

Files: contrib/statusd/legacy/statusd_apm.lua
Copyright: 2006 Greg Steuck
           2006 Darrin Chandler <dwchandler@stilyagin.com>
License: public-domain

Files: contrib/statusd/legacy/statusd_apm2.lua
Copyright: Greg Steuck
           Gerald Young <ion3script@gwy.org>
License: public-domain

Files: contrib/statusd/legacy/statusd_info.lua
       contrib/statusd/legacy/statusd_volume2.lua
Copyright: Randall Wald <randy@rwald.com>
License: GPL-2

Files: contrib/statusd/legacy/statusd_linuxbatt.lua
       contrib/statusd/statusd_cpufreq.lua
       contrib/statusd/statusd_moc.lua
       contrib/statusd/statusd_linuxbatt.lua
Copyright: Unknown
License: public-domain

Files: contrib/statusd/statusd_bitcoin.lua
       contrib/statusd/statusd_drives.lua
       contrib/statusd/statusd_xmms2.lua
Copyright: Voker57 <voker57@gmail.com>
License: public-domain

Files: contrib/statusd/statusd_cpustat.lua
       contrib/styles/look_tibi.lua
Copyright: 2007 Tibor Csögör <tibi@tiborius.net>
License: public-domain

Files: contrib/statusd/statusd_df.lua
       contrib/statusd/statusd_meminfo.lua
Copyright: 2006 Tibor Csögör <tibi@tiborius.net>
License: public-domain

Files: contrib/statusd/statusd_fortune.lua
       contrib/statusd/statusd_mocmon.lua
       contrib/statusd/statusd_xmmsip.lua
Copyright: 2006 Hendrik Iben <hiben@tzi.de>
License: GPL-2

Files: contrib/statusd/statusd_inetaddr2/statusd_inetaddr2.lua
Copyright: Guillermo Bonvehí <gbonvehi@gmail.com>
License: public-domain

Files: contrib/statusd/statusd_nginfo.lua
Copyright: Raffaello Pelagalli <raffa@niah.org>
License: LGPL-2.1+

Files: contrib/statusd/statusd_volume.lua
Copyright: Benjamin Sigonneau
License: public-domain

Files: contrib/statusd/statusd_xmms.lua
Copyright: Peter Randeu <ranpet@sbox.tugraz.at>
License: GPL-2

Files: ioncore/modules.*
Copyright: 2011 Arnout Engelen <arnouten@bzzt.net>
           1999-2007 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: ioncore/frame-tabs-recalc.*
Copyright: 2011 Tomas Ebenlendr <ebik@ucw.cz>
           Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: po/cs.po
Copyright: 2004-2007 Miroslav Kure <kurem@debian.cz>
License: LGPL-2.1+

Files: po/fi.po
Copyright: 2004 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

Files: test/integration/Xdummy
Copyright: 2005-2011 Karl J. Runge <runge@karlrunge.com>
License: GPL-2+

Files: test/integration/Xinerama.c
Copyright: 1991,1997 Digital Equipment Corporation
License: MIT

Files: mod_xrandr/*
Copyright: 2004 Ragnar Rova
           2005-2007 Tuomo Valkonen <tuomov@iki.fi>
License: LGPL-2.1+

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 On Debian GNU/Linux systems, the complete text of the General Public
 License 2 can be found in `/usr/share/common-licenses/GPL-2'.
 
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the General Public
 License 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the Lesser General
 Public License 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1'.

License: MIT-old-style
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that the
 above copyright notice appear in all copies and that both that copyright notice
 and this permission notice appear in supporting documentation, and that the
 name of M.I.T. not be used in advertising or publicity pertaining to
 distribution of the software without specific, written prior permission. M.I.T.
 makes no representations about the suitability of this software for any
 purpose. It is provided "as is" without express or implied warranty.

License: NetBSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of 
 this software and associated documentation files (the "Software"), to deal in 
 the Software without restriction, including without limitation the rights to 
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
 of the Software, and to permit persons to whom the Software is furnished to do 
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all 
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 SOFTWARE.

License: BSD-completefile
 Permission is granted to anyone to use this software for any purpose on
 any computer system, and to alter it and redistribute it freely, subject
 to the following restrictions:
 1. The authors are not responsible for the consequences of use of this
    software, no matter how awful, even if they arise from flaws in it.
 2. The origin of this software must not be misrepresented, either by
    explicit claim or by omission.  Since few users ever read sources,
    credits must appear in the documentation.
 3. Altered versions must be plainly marked as such, and must not be
    misrepresented as being the original software.  Since few users
    ever read sources, credits must appear in the documentation.
 4. This notice may not be removed or altered.

License: Artistic
 The Artistic License
 .
 Preamble
 .
 The intent of this document is to state the conditions under which a Package may
 be copied, such that the Copyright Holder maintains some semblance of artistic
 control over the development of the package, while giving the users of the
 package the right to use and distribute the Package in a more-or-less customary
 fashion, plus the right to make reasonable modifications.
 .
 Definitions:
 .
     "Package" refers to the collection of files distributed by the Copyright
     Holder, and derivatives of that collection of files created through textual
     modification. "Standard Version" refers to such a Package if it has not been
     modified, or has been modified in accordance with the wishes of the
     Copyright Holder. "Copyright Holder" is whoever is named in the copyright or
     copyrights for the package. "You" is you, if you're thinking about copying
     or distributing this Package. "Reasonable copying fee" is whatever you can
     justify on the basis of media cost, duplication charges, time of people
     involved, and so on. (You will not be required to justify it to the
     Copyright Holder, but only to the computing community at large as a market
     that must bear the fee.) "Freely Available" means that no fee is charged for
     the item itself, though there may be fees involved in handling the item. It
     also means that recipients of the item may redistribute it under the same
     conditions they received it.
 .
 1. You may make and give away verbatim copies of the source form of the Standard
 Version of this Package without restriction, provided that you duplicate all of
 the original copyright notices and associated disclaimers.
 .
 2. You may apply bug fixes, portability fixes and other modifications derived
 from the Public Domain or from the Copyright Holder. A Package modified in such
 a way shall still be considered the Standard Version.
 .
 3. You may otherwise modify your copy of this Package in any way, provided that
 you insert a prominent notice in each changed file stating how and when you
 changed that file, and provided that you do at least ONE of the following:
 .
     a) place your modifications in the Public Domain or otherwise make them
     Freely Available, such as by posting said modifications to Usenet or an
     equivalent medium, or placing the modifications on a major archive site such
     as ftp.uu.net, or by allowing the Copyright Holder to include your
     modifications in the Standard Version of the Package.
 .
     b) use the modified Package only within your corporation or organization.
 .
     c) rename any non-standard executables so the names do not conflict with
     standard executables, which must also be provided, and provide a separate
     manual page for each non-standard executable that clearly documents how it
     differs from the Standard Version.
 .
     d) make other distribution arrangements with the Copyright Holder.
 .
 4. You may distribute the programs of this Package in object code or executable
 form, provided that you do at least ONE of the following:
 .
     a) distribute a Standard Version of the executables and library files,
     together with instructions (in the manual page or equivalent) on where to
     get the Standard Version.
 .
     b) accompany the distribution with the machine-readable source of the
     Package with your modifications.
 .
     c) accompany any non-standard executables with their corresponding Standard
     Version executables, giving the non-standard executables non-standard names,
     and clearly documenting the differences in manual pages (or equivalent),
     together with instructions on where to get the Standard Version.
 .
     d) make other distribution arrangements with the Copyright Holder.
 .
 5. You may charge a reasonable copying fee for any distribution of this Package.
 You may charge any fee you choose for support of this Package. You may not
 charge a fee for this Package itself. However, you may distribute this Package
 in aggregate with other (possibly commercial) programs as part of a larger
 (possibly commercial) software distribution provided that you do not advertise
 this Package as a product of your own.
 .
 6. The scripts and library files supplied as input to or produced as output from
 the programs of this Package do not automatically fall under the copyright of
 this Package, but belong to whomever generated them, and may be sold
 commercially, and may be aggregated with this Package.
 .
 7. C or perl subroutines supplied by you and linked into this Package shall not
 be considered part of this Package.
 .
 8. The name of the Copyright Holder may not be used to endorse or promote
 products derived from this software without specific prior written permission.
 .
 9. THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 .
 The End

License: Artistic_Clarified
 The Clarified Artistic License
 .
 Preamble
 .
 The intent of this document is to state the conditions under which a Package
 may be copied, such that the Copyright Holder maintains some semblance of
 artistic control over the development of the package, while giving the users of
 the package the right to use and distribute the Package in a more-or-less
 customary fashion, plus the right to make reasonable modifications.
 .
 Definitions:
 .
 "Package" refers to the collection of files distributed by the Copyright
 Holder, and derivatives of that collection of files created through textual
 modification.
 .
 "Standard Version" refers to such a Package if it has not been modified, or has
 been modified in accordance with the wishes of the Copyright Holder as
 specified below.
 .
 "Copyright Holder" is whoever is named in the copyright or copyrights for the
 package.
 .
 "You" is you, if you're thinking about copying or distributing this Package.
 .
 "Distribution fee" is a fee you charge for providing a copy of this Package to
 another party.
 .
 "Freely Available" means that no fee is charged for the right to use the item,
 though there may be fees involved in handling the item. It also means that
 recipients of the item may redistribute it under the same conditions they
 received it.
 .
 1. You may make and give away verbatim copies of the source form of the
 Standard Version of this Package without restriction, provided that you
 duplicate all of the original copyright notices and associated disclaimers.
 .
 2. You may apply bug fixes, portability fixes and other modifications derived
 from the Public Domain, or those made Freely Available, or from the Copyright
 Holder. A Package modified in such a way shall still be considered the Standard
 Version.
 .
 3. You may otherwise modify your copy of this Package in any way, provided that
 you insert a prominent notice in each changed file stating how and when you
 changed that file, and provided that you do at least ONE of the following:
 .
 a) place your modifications in the Public Domain or otherwise make them Freely
 Available, such as by posting said modifications to Usenet or an equivalent
 medium, or placing the modifications on a major network archive site allowing
 unrestricted access to them, or by allowing the Copyright Holder to include
 your modifications in the Standard Version of the Package.
 .
 b) use the modified Package only within your corporation or organization.
 .
 c) rename any non-standard executables so the names do not conflict with
 standard executables, which must also be provided, and provide a separate
 manual page for each non-standard executable that clearly documents how it
 differs from the Standard Version.
 .
 d) make other distribution arrangements with the Copyright Holder.
 .
 e) permit and encourge anyone who receives a copy of the modified Package
 permission to make your modifications Freely Available in some specific way.
 .
 4. You may distribute the programs of this Package in object code or executable
 form, provided that you do at least ONE of the following:
 .
 a) distribute a Standard Version of the executables and library files, together
 with instructions (in the manual page or equivalent) on where to get the
 Standard Version.
 .
 b) accompany the distribution with the machine-readable source of the Package
 with your modifications.
 .
 c) give non-standard executables non-standard names, and clearly document the
 differences in manual pages (or equivalent), together with instructions on
 where to get the Standard Version.
 .
 d) make other distribution arrangements with the Copyright Holder.
 .
 e) offer the machine-readable source of the Package, with your modifications,
 by mail order.
 .
 5. You may charge a distribution fee for any distribution of this Package. If
 you offer support for this Package, you may charge any fee you choose for that
 support. You may not charge a license fee for the right to use this Package
 itself. You may distribute this Package in aggregate with other (possibly
 commercial and possibly nonfree) programs as part of a larger (possibly
 commercial and possibly nonfree) software distribution, and charge license fees
 for other parts of that software distribution, provided that you do not
 advertise this Package as a product of your own. If the Package includes an
 interpreter, You may embed this Package's interpreter within an executable of
 yours (by linking); this shall be construed as a mere form of aggregation,
 provided that the complete Standard Version of the interpreter is so embedded.
 .
 6. The scripts and library files supplied as input to or produced as output
 from the programs of this Package do not automatically fall under the copyright
 of this Package, but belong to whoever generated them, and may be sold
 commercially, and may be aggregated with this Package. If such scripts or
 library files are aggregated with this Package via the so-called "undump" or
 "unexec" methods of producing a binary executable image, then distribution of
 such an image shall neither be construed as a distribution of this Package nor
 shall it fall under the restrictions of Paragraphs 3 and 4, provided that you
 do not represent such an executable image as a Standard Version of this
 Package.
 .
 7. C subroutines (or comparably compiled subroutines in other languages)
 supplied by you and linked into this Package in order to emulate subroutines
 and variables of the language defined by this Package shall not be considered
 part of this Package, but are the equivalent of input as in Paragraph 6,
 provided these subroutines do not change the language in any way that would
 cause it to fail the regression tests for the language.
 .
 8. Aggregation of the Standard Version of the Package with a commercial
 distribution is always permitted provided that the use of this Package is
 embedded; that is, when no overt attempt is made to make this Package's
 interfaces visible to the end user of the commercial distribution. Such use
 shall not be construed as a distribution of this Package.
 .
 9. The name of the Copyright Holder may not be used to endorse or promote
 products derived from this software without specific prior written permission.
 .
 10. THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 .
 The End

License: Artistic_Frontier
 The Frontier Artistic License Version 1.0 Derived from the Artistic License at
 OpenSource.org. Submitted to OpenSource.org for Open Source Initiative
 certification.
 .
 Preamble
 .
 The intent of this document is to state the conditions under which a Package may
 be copied, such that the Copyright Holder maintains some semblance of artistic
 control over the development of the package, while giving the users of the
 package the right to use and distribute the Package in a more-or-less customary
 fashion, plus the right to make reasonable modifications.
 .
 Definitions
 .
   "Package" refers to the script, suite, file, or collection of scripts, suites,
   and/or files distributed by the Copyright Holder, and to derivatives of that
   Package created through textual modification.
 .
   "Standard Version" refers to such a Package if it has not been modified, or
   has been modified in accordance with the wishes of the Copyright Holder.
 .
   "Copyright Holder" is whoever is named in the copyright statement or
   statements for the package.
 .
   "You" is you, if you're thinking about copying or distributing this Package.
 .
   "Reasonable copying fee" is whatever you can justify on the basis of media
   cost, duplication charges, time of people involved, and so on. (You will not
   be required to justify it to the Copyright Holder, but only to the computing
   community at large as a market that must bear the fee.)
 .
   "Freely Available" means that no fee is charged for the item itself, though
   there may be fees involved in handling the item. It also means that recipients
   of the item may redistribute it under the same conditions they received it.
 .
 .
 Terms
 .
 1. You may make and give away verbatim copies of the source form of the Standard
 Version of this Package without restriction, provided that you duplicate all of
 the original copyright notices and associated disclaimers.
 .
 2. You may apply bug fixes, portability fixes, and other modifications derived
 from the Public Domain or from the Copyright Holder. A Package modified in such
 a way shall still be considered the Standard Version.
 .
 3. You may otherwise modify your copy of this Package in any way, provided that
 you insert a prominent notice in each changed script, suite, or file stating how
 and when you changed that script, suite, or file, and provided that you do at
 least ONE of the following:
 .
   a) Use the modified Package only within your corporation or organization, or
   retain the modified Package solely for personal use.
 .
   b) Place your modifications in the Public Domain or otherwise make them Freely
   Available, such as by posting said modifications to Usenet or an equivalent
   medium, or placing the modifications on a major archive site such as
   ftp.uu.net, or by allowing the Copyright Holder to include your modifications
   in the Standard Version of the Package.
 .
   c) Rename any non-standard executables so the names do not conflict with
   standard executables, which must also be provided, and provide a separate
   manual page (or equivalent) for each non-standard executable that clearly
   documents how it differs from the Standard Version.
 .
   d) Make other distribution arrangements with the Copyright Holder.
 .
 4. You may distribute the programs of this Package in object code or executable
 form, provided that you do at least ONE of the following:
 .
   a) Distribute a Standard Version of the executables and library files,
   together with instructions (in the manual page or equivalent) on where to get
   the Standard Version.
 .
   b) Accompany the distribution with the machine-readable source of the Package
   with your modifications.
 .
   c) Accompany any non-standard executables with their corresponding Standard
   Version executables, give the non-standard executables non-standard names, and
   clearly document the differences in manual pages (or equivalent), together
   with instructions on where to get the Standard Version.
 .
   d) Make other distribution arrangements with the Copyright Holder.
 .
 5. You may charge a reasonable copying fee for any distribution of this Package.
 You may charge any fee you choose for support of this Package. You may not
 charge a fee for this Package itself. However, you may distribute this Package
 in aggregate with other (possibly commercial) programs as part of a larger
 (possibly commercial) software distribution provided that you do not advertise
 this Package as a product of your own.
 .
 6. The scripts and library files supplied as input to or produced as output from
 the programs of this Package do not automatically fall under the copyright of
 this Package, but belong to whomever generated them, and may be sold
 commercially, and may be aggregated with this Package.
 .
 7. Scripts, suites, or programs supplied by you that depend on or otherwise make
 use of this Package shall not be considered part of this Package.
 .
 8. The name of the Copyright Holder may not be used to endorse or promote
 products derived from this software without specific prior written permission.
 .
 9. THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 .
                         The End

License: public-domain-emacs-bindings
 Files tagged with this license say:
 .
 Authors: Matthieu MOY <Matthieu.Moy@imag.fr>,
          David Hansen <david.hansen@physik.fu-berlin.de>,
          Tyranix <tyranix@gmail.com>,
          Adam Di Carlo <aph@debian.org>
 License: public-domain
 Last Changed: 2007-04-27
 .
 Emacs-like keyboard configuration for Ion, version 3.
 Written by Matthieu MOY (Matthieu.Moy@imag.fr) on February 15, 2005.
 No copyright.

License: public-domain-xkbion
 Files tagged with this license say:
 .
 Authors: Sergey Redin <sergey@redin.info>
 License: public-domain
 Last Changed: Unknown
 .
 Author: Sergey Redin <sergey at redin.info>
 This software is in the public domain.
